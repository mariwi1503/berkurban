"use strict"

require('dotenv').config()

let config = {
    port: process.env.PORT || 3000,
    mysql: {
        host: process.env.DB_HOST,
        database: process.env.DB_NAME,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD
    },
    token_secret: process.env.TOKEN_SECRET
}

module.exports = config