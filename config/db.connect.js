"use strict"

const config = require('./index')
//     , { Sequelize, DataType } = require('sequelize')

// let db = new Sequelize(
//     config.mysql.database,
//     config.mysql.user,
//     config.mysql.password,
//     {
//         host: config.mysql.host,
//         dialect: 'mysql'
//     }
// )

// db.authenticate()
// .then(() => console.log('Database connected'))
// .catch((err) => {
//     throw new Error(err)
// })

// // sinkronisasi database hanya diaktifkan ketika ada perubahan dan tambahan di folder model, setelah itu dikomen lagi
// db.sync()

// module.exports = db;

const mysql = require('mysql2')

const pool = mysql.createPool({
    host: config.mysql.host,
    user: config.mysql.user,
    password: config.mysql.password,
    database: config.mysql.database
}).promise()

module.exports = pool;