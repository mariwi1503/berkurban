"use strict"

const customerModel = require('../models/customer.model')

module.exports = {
    getCustomerList: async (req, res) => {
        try {
            let result = await customerModel.getAllCustomer()
            result = result.length > 0 ? result : null

            res.status(200).json({
                status: 'success',
                data: result
            })
        } catch (error) {
            res.status(500).json({
                status: 'failed',
                message: error.message
            })
        }
    },
    getCustomerDetails: async (req, res) => {
        try {
            let id = req.customer_id
            let result = await customerModel.getCustomerDetail(id)
            if(!result) throw new Error('Data tidak ditemukan')

            res.status(200).json({
                status: 'success',
                data: result
            })
        } catch (error) {
            res.status(500).json({
                status: 'failed',
                message: error.message
            })
        }
    },
}