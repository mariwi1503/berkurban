"use strict"

const pool = require('../config/db.connect')
    , moment = require('moment')
    , async = require('async')
    , orderModel = require('../models/order.model')
    , productModel = require('../models/product.model')

module.exports = {
    createOrder: async (req, res) => {
        let conn = await pool.getConnection()
        try {
            let { payment_type = 'cod',total_price, items } = req.body
            // items = array dari product_id
            let customer_id = req.customer_id
            let invoice = "T" + customer_id + "-" + moment().format('MMDD') + "-" + moment().format('mmss')
            let order_items = []
            let sold_out_item = []
            let total = 0;
            await async.forEachOf(items, async (x) => {
                let product  = await productModel.getDetailProduct(x)
                total += product.price
                if(product.sold_out == 1) {
                    sold_out_item.push(product.id)
                } else {
                    order_items.push({
                        product_id: product.id,
                        product_type: product.type,
                        price: product.price
                    })
                }
            })

            if(total_price != total) throw new Error('Total belanja salah')
            if(sold_out_item.length > 0) throw new Error(`Product dengan id ${sold_out_item.join()} sudah terjual`)

            let order_data = {
                customer_id,
                invoice,
                payment_type,
                total_price
            }

            // submit using transaction mysql
            await conn.beginTransaction()
            let order_created = await orderModel.createOrder(conn, order_data)
            await orderModel.addOrderItems(conn, order_created.insertId, order_items)

            // set product soldout
            await async.forEachOf(items, async (x) => {
                await productModel.setSoldOut(conn, x)
            })

            await conn.commit()
            await conn.release()

            res.status(201).json({
                status: 'success'
            })

        } catch (error) {
            console.log(error)
            await conn.rollback()
            await conn.release()
            res.status(500).json({
                status: 'failed',
                message: error.message
            })
        }
    }
}