"use strict"

const productModel = require('../models/product.model')

module.exports = {
    getAllProduct: async (req, res) => {
        try {
            let result = await productModel.getAllProduct()
            result = result ? result : null

            res.status(200).json({
                status: 'success',
                data: result
            })
        } catch (error) {
            res.status(500).json({
                status: 'failed',
                message: error.message
            })
        }
    },
    getProductDetail: async (req, res) => {
        try {
            let id = req.params.id
            let product = await productModel.getDetailProduct(id)
            if(!product) throw new Error('Product tidak ditemukan')

            res.status(200).json({
                status: 'success',
                data: product
            })
        } catch (error) {
            res.status(500).json({
                status: 'failed',
                message: error.message
            })
        }
    },
    addProduct: async (req, res) => {
        let { type, grade, price, gender } = req.body
        try {
            await productModel.addProduct({
                type, grade, price, gender
            })

            res.status(201).json({
                status: 'success'
            })
        } catch (error) {
            res.status(500).json({
                status: 'failed',
                message: error.message
            })
        }
    },
}