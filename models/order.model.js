"use strict"

const pool = require('../config/db.connect')

module.exports = {
    createOrder: async (conn, data) => {
        try {
            let [rows] = await conn.query(`insert into orders set ?`, data)
            return rows
        } catch (error) {
            throw new Error(error)
        }
    },
    addOrderItems: async (conn, order_id, data) => {
        try {
            let q = `insert into order_items (order_id, product_id, product_type, price) values `
            await data.map((x) => {
                q += `(${(order_id)}, ${x.product_id}, ${pool.escape(x.product_type)}, ${x.price}),`
            })
            let [rows] = await conn.query(q.substring(0, q.length - 1))
            return rows
        } catch (error) {
            throw new Error(error)
        }
    }
}