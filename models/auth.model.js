"use strict"

const pool = require('../config/db.connect')

module.exports = {
    getCustomerByPhone: async (phone) => {
        try {
            let query = `select * from customers where phone = ?`
            let [[rows]] = await pool.query(query, [phone])
            return rows
        } catch (error) {
            throw new Error(error)
        }
    },
    signUp: async (data) => {
        try {
            let query = 'insert into customers set ?'
            let [rows] = await pool.query(query, data)
            return rows
        } catch (error) {
            throw new Error(error)
        }
    },
    
}