-- [ table create ] --

-- Table customers
CREATE TABLE `dataku`.`coba` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(100) NOT NULL , `phone` VARCHAR(15) NOT NULL , `password` VARCHAR(225) NOT NULL , `level` ENUM('user','admin') NOT NULL DEFAULT 'user' , `area` VARCHAR(225) NULL DEFAULT NULL , `refferal_id` INT NULL DEFAULT NULL , `interval_otp` INT NULL DEFAULT NULL , `status` ENUM('1','-1') NOT NULL DEFAULT '1' COMMENT '1=aktif, -1=banned' , `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`), UNIQUE `phone` (`phone`)) ENGINE = InnoDB;

-- Table products
CREATE TABLE `dataku`.`products` ( `id` INT NOT NULL AUTO_INCREMENT , `type` ENUM('sapi','lembu','kerbau','kambing','domba') NOT NULL , `grade` ENUM('s','m','l','xl') NOT NULL , `price` INT NOT NULL DEFAULT '0' , `gender` ENUM('male','female') NOT NULL , `sold_out` TINYINT NULL DEFAULT NULL COMMENT '1=sold_out' , `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- Table orders
CREATE TABLE `dataku`.`orders` ( `id` INT NOT NULL AUTO_INCREMENT , `payment_id` INT NOT NULL DEFAULT '0' , `customer_id` INT NOT NULL , `invoice` VARCHAR(100) NOT NULL , `payment_type` ENUM('cod','transfer') NOT NULL , `status` INT NOT NULL DEFAULT '1' , `total_price` INT NOT NULL DEFAULT '0' , `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- Table order_items
CREATE TABLE `dataku`.`order_items` ( `id` INT NOT NULL AUTO_INCREMENT , `order_id` INT NOT NULL , `product_id` INT NOT NULL , `product_type` ENUM('sapi','lembu','kerbau','kambing','domba') NOT NULL , `price` INT NOT NULL DEFAULT '0' , `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;