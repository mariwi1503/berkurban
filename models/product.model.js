"use strict"

const pool = require('../config/db.connect')

module.exports = {
    getAllProduct: async () => {
        try {
            let q = 'select * from products'
            let [rows] = await pool.query(q)
            return rows
        } catch (error) {
            throw new Error(error)
        }
    },
    getDetailProduct: async (id) => {
        try {
            let q = 'select * from products where id = ?'
            let [[rows]] = await pool.query(q, id)
            return rows
        } catch (error) {
            throw new Error(error)
        }
    },
    addProduct: async (data) => {
        try {
            let [rows] = await pool.query(`insert into products set ?`, data)
        } catch (error) {
            throw new Error(error)
        }
    },
    setSoldOut: async (conn, product_id) => {
        try {
            let [rows] = await conn.query('update products set ? where id = ?', [{sold_out: 1}, product_id])
            return rows
        } catch (error) {
            throw new Error(error)
        }
    }
}