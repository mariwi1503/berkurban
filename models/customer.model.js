"use strict"

const pool = require('../config/db.connect')

module.exports = {
    getAllCustomer: async () => {
        try {
            let [rows] = pool.query('select * from customers')
            return rows
        } catch (error) {
            throw new Error(error)
        }
    },
    getCustomerDetail: async (id) => {
        try {
            let q = 'select * from customers where id = ?'
            let [[rows]] = await pool.query(q, [id])
            return rows
        } catch (error) {
            throw new Error(error)
        }
    },
    getCustomerById: async (id) => {
        try {
            let q = 'select * from customers where id = ?'
            let [[rows]] = pool.query(q, id)
            return rows
        } catch (error) {
            throw new Error(error)
        }
    },
    updateCustomer: async (data) => {
        try {
            let q = 'update customers set ? where id = ?'
            let [rows] = await pool.query(q, data)
        } catch (error) {
            throw new Error(error)
        }
    },
}