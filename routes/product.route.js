"use strict"

const router = require('express').Router()
    , productController = require('../controllers/product.controller')
    , authentication = require('../middleware/authentication')

// routes
router.get('/product/list', authentication.allUser, productController.getAllProduct)
router.post('/product/add', authentication.admin, productController.addProduct)
router.get('/product/:id', authentication.allUser, productController.getProductDetail)

module.exports = router;