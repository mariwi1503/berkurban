"use strict"

const router = require('express').Router()
    , authentication = require('../middleware/authentication')
    , orderController = require('../controllers/order.controller')

router.post('/order/create', authentication.allUser, orderController.createOrder)

module.exports = router;