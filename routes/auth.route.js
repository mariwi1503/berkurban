"use strict"

const router = require('express').Router()
    , authController = require('../controllers/auth.controller')

router.get('/auth/check', authController.phoneCheck)
router.post('/auth/signup', authController.signup)
router.post('/auth/login', authController.loginPassword)

module.exports = router