"use strict"

const router = require('express').Router()
    , customerController = require('../controllers/customer.controller')
    , authentication = require('../middleware/authentication')

// routes
router.get('/customer/list', authentication.admin, customerController.getCustomerList)
router.get('/customer/info', authentication.allUser, customerController.getCustomerDetails)

module.exports = router