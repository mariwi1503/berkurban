"use strict"

const express = require('express')
    , config = require('./config')
    , morgan = require('morgan')
    , authRoute = require('./routes/auth.route')
    , customerRoute = require('./routes/customer.route')
    , productRoute = require('./routes/product.route')
    , orderRoute = require('./routes/order.route')

const port = config.port
const app = express()

app.use(express.json())
app.use(morgan('dev'))

// routes

app.use(
    '/api',
    authRoute,
    customerRoute,
    productRoute,
    orderRoute
)

// global route
app.get('/', (req, res) => {
    res.send('Wellcome to berkurban API')
})

// unhandled route
app.all('*', (req, res) => {
    res.send('Sepertinya anda tersesat')
})

app.listen(port, () => {
    console.log(`Server running on post: ${port}`)
})