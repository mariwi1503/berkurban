"use strict"

const config = require('../config')

module.exports = {
    generateToken: (digit) => {
        // generate 4 digit random number
        let code = []
        for (var i = 0; i < digit; i++) {
          var number = Math.floor(Math.random() * 9) + 1
          code.push(number)
        }
        code = code.join('')
        return code
    },
    sendOTP: async (phone, token) => {
        return console.log(token)
    },
}